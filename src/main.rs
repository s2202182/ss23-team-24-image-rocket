#[macro_use] extern crate rocket;
extern crate image;

use rocket::figment::{value::{Map, Value}, util::map};
use std::{collections::HashMap, path::PathBuf};
use rocket::{fs::{FileServer, TempFile}, fairing::{AdHoc}, Rocket, Build, serde::{Serialize, Deserialize}, response::Redirect, form::Form, tokio::io::AsyncReadExt, http::ContentType};
use rocket_dyn_templates::{Template};
use rocket_sync_db_pools::{postgres, database};
use image::{GenericImageView};
use image::io::Reader as ImageReader;

use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;
use rocket::{Request, Response};

pub struct CSP;

#[rocket::async_trait]
impl Fairing for CSP {
    fn info(&self) -> Info {
        Info {
            name: "Content Security Policy",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Content-Security-Policy", "default-src 'self'; img-src 'self' data:;"));
    }
}


#[database("postgres")]
struct MyPgDatabase(postgres::Client);

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct ImagesList {
    images: Vec<ImageListItem>
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct ImageListItem {
    id: i32,
    path: String,
    width: i32,
    height: i32,
    title: String
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct ImageShow {
    title: String,
    path: String,
    id: i32,
    comments: Vec<ImageComment>,
    metadata: Option<ImageMetadata>
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct ImageResult {
    id: i32,
    title: String,
    path: String,
    width: i32,
    height: i32
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct SearchResult {
    query: String,
    results: Vec<ImageResult>
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct ImageComment {
    text: String,
    user: String
}

#[derive(FromForm)]
struct PostComment {
    comment: String,
    user_name: String
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct ImageMetadata {
    creation_time: f64,
    camera_make: String,
    camera_model: String,
    orientation: i32,
    horizontal_ppi: i32,
    vertical_ppi: i32,
    shutter_speed: f64,
    color_space: String
}

#[derive(FromForm)]
struct UploadImage<'v> {
    title: String,
    private: bool,
    file: TempFile<'v>,
    metadata: Option<TempFile<'v>>
}

async fn get_xml_tag(filename: &String, tagname: &String) -> String {
    let output = tokio::process::Command::new("xmllint")
        .arg("--noent")
        .arg("--xpath")
        .arg(format!("//MetaData/{}/text()", tagname))
        .arg(filename)
        .output()
        .await
        .unwrap();
    let stdout = output.stdout;
    String::from_utf8(stdout).unwrap()
}

async fn read_image(filename: &PathBuf) -> (i32, i32, Vec<u8>) {
    let mut fh = rocket::tokio::fs::File::open(filename).await.unwrap();
    let mut buf = Vec::new();
    fh.read_to_end(&mut buf).await.unwrap();
    let image = ImageReader::new(std::io::Cursor::new(&buf)).with_guessed_format().unwrap().decode().unwrap();
    let width = i32::try_from(image.width()).ok().unwrap();
    let height = i32::try_from(image.height()).ok().unwrap();
    drop(fh);
    return (width, height, buf);
}

#[post("/image/<imageid>/comments/post", data= "<comment>")]
async fn comment(conn: MyPgDatabase, imageid: i32, comment: Form<PostComment>) -> Redirect {
    use ammonia::clean;

    let sanitized_comment = clean(&comment.comment);
    conn.run(move |conn| {
        conn.query("INSERT INTO comments (image_id, user_name, comment) VALUES ($1, $2, $3)", 
            &[& imageid, &comment.user_name, &sanitized_comment])
    }).await.unwrap();
    Redirect::to(format!("/image/{}", imageid))
}


#[post("/upload", data = "<form>")]
async fn upload_post(conn: MyPgDatabase, mut form: Form<UploadImage<'_>>) -> Redirect {
    let some_path = std::env::temp_dir().join(form.file.name().unwrap());
    form.file.persist_to(&some_path).await.unwrap();
    let (mut width, mut height, mut buf) = read_image(&some_path).await;
    if std::cmp::max(width, height) > 2048 {
        let command = format!(
            "convert -scale 2048x2048 -quality 90 {} {}/out.jpg; cp {}/out.jpg {}; rm {}/out.jpg", 
            &some_path.display(), 
            std::env::temp_dir().display(), 
            std::env::temp_dir().display(), 
            &some_path.display(), 
            std::env::temp_dir().display()
        );
        println!("{}", &command);
        let _command_result = tokio::process::Command::new("sh")
            .arg("-c")
            .arg(&command)
            .spawn()
            .unwrap()
            .wait()
            .await;
        let (newwidth, newheight, newbuf) = read_image(&some_path).await;
        buf = newbuf;
        height = newheight;
        width = newwidth;
    }
    rocket::tokio::fs::remove_file(&some_path).await.unwrap();

    let title = form.title.clone();
    let path = String::from(form.file.name().unwrap());
    let private = form.private.clone();
    let r = conn.run(move |conn| {
        conn.query("INSERT INTO images (title, path, width, height, private, content) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id", 
            &[& title, &path, &width, &height, &private, &buf])
        
    }).await;
    let id: i32;
    match r {
        Ok(v) => {
            id = v.get(0).unwrap().get(0);
        },
        Err(e) => {
            eprintln!("{}", e);
            return Redirect::to("/")
        },
    }
    
    if let Some(metadata) = &mut form.metadata {
        let name_result = metadata.name();
        if name_result.is_some() {
            let name = name_result.unwrap();
            let metadata_path = format!("{}", std::env::temp_dir().join(name).display());
            metadata.persist_to(&metadata_path).await.unwrap();
            
            let creation_time: f64 = get_xml_tag(&metadata_path, &String::from("creationTime")).await.parse().unwrap_or_else(|_| 0.0);
            let camera_make = get_xml_tag(&metadata_path, &String::from("cameraMake")).await;
            let camera_model = get_xml_tag(&metadata_path, &String::from("cameraModel")).await;
            let orientation: i32 = get_xml_tag(&metadata_path, &String::from("orientation")).await.parse().unwrap_or_else(|_| 0);
            let horizontal_ppi: i32 = get_xml_tag(&metadata_path, &String::from("horizontalPpi")).await.parse().unwrap_or_else(|_| 0);
            let vertical_ppi: i32 = get_xml_tag(&metadata_path, &String::from("verticalPpi")).await.parse().unwrap_or_else(|_| 0);
            let shutter_speed: f64 = get_xml_tag(&metadata_path, &String::from("shutterSpeed")).await.parse().unwrap_or_else(|_| 0.0);
            let color_space = get_xml_tag(&metadata_path, &String::from("colorSpace")).await;

            rocket::tokio::fs::remove_file(&metadata_path).await.unwrap();
            conn.run(move |conn| {
                conn.query("INSERT INTO metadata (image_id, creationtime, camera_make, camera_model, orientation, horizontal_ppi, vertical_ppi, shutter_speed, color_space) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)", 
                &[&id, &creation_time, &camera_make, &camera_model, &orientation, &horizontal_ppi, &vertical_ppi, &shutter_speed, &color_space])
                
            }).await.unwrap();
        }

    }
    Redirect::to(format!("/image/{}", id))
}

#[get("/upload")]
async fn upload() -> Template {
    let context: HashMap<String, String> = HashMap::new();
    Template::render("upload", context)
}

#[get("/search?<q>")]
async fn search(conn: MyPgDatabase, q: String) -> Template {
    let mut results: Vec<ImageResult> = Vec::new();
    let qs = format!("SELECT id, path, width, height, title FROM images WHERE NOT private AND (to_tsvector('simple', title) @@ plainto_tsquery('simple', '{}'))", q);
    let r = conn.run(move |conn| {
        conn.query(&qs, &[])
        
    }).await.unwrap();
    for row in r.iter() {
        results.push(ImageResult { id: row.get(0), path: row.get(1), width: row.get(2), height: row.get(3), title: row.get(4) });
    }
    let result = SearchResult { query: q, results: results };
    Template::render("search", result)
}

#[get("/image/<imageid>")]
async fn images_name(conn: MyPgDatabase, imageid: i32) -> Template {
    let r = conn.run(move |conn| {
        conn.query_one("SELECT path, title, id FROM images WHERE id = $1", &[&imageid])
        
    }).await.unwrap();
    let path: String = r.get(0);
    let title: String = r.get(1);
    let id: i32 = r.get(2);

    let metadata_result = conn.run(move |conn| {
        conn.query_one("SELECT creationtime, camera_make, camera_model, orientation, horizontal_ppi, vertical_ppi, shutter_speed, color_space FROM metadata WHERE image_id = $1", &[&imageid])
        
    }).await;

    let comment_result = conn.run(move |conn| {
        conn.query("SELECT user_name, comment from comments where image_id = $1", &[&imageid])
    }).await.unwrap();
    let mut comments: Vec<ImageComment> = Vec::new();
    for comment in comment_result.iter() {
        comments.push(ImageComment {
            user: comment.get(0),
            text: comment.get(1)
        })
    }
    let metadata = match metadata_result {
        Ok(row) => Some(ImageMetadata {
            creation_time: row.get(0),
            camera_make: row.get(1),
            camera_model: row.get(2),
            orientation: row.get(3),
            horizontal_ppi: row.get(4),
            vertical_ppi: row.get(5),
            shutter_speed: row.get(6),
            color_space: row.get(7)
        }),
        Err(_) => None
    };
    let context = ImageShow {
        title: title,
        path: path,
        id: id,
        comments: comments,
        metadata: metadata
    };
    Template::render("image", context)
}

#[get("/img/<name>")]
async fn img(conn: MyPgDatabase, name: String) -> (ContentType, Vec<u8>) {
    let r = conn.run(move |conn| {
        conn.query_one("SELECT content from images where path = $1 LIMIT 1", &[&name])
        
    }).await.unwrap();
    let content = r.get(0);
    return (ContentType::JPEG, content);
}

#[get("/")]
async fn images(conn: MyPgDatabase) -> Template {
    let r = conn.run(|conn| {
        conn.query("SELECT id, path, width, height, title FROM images WHERE NOT private", &[])
    }).await;

    match r {
        Ok(l) => {
            let image_list = l.iter().map(|row| -> ImageListItem {
                ImageListItem {
                    id: row.get(0),
                    path: row.get(1),
                    width: row.get(2),
                    height: row.get(3),
                    title: row.get(4)
                }
            }).collect::<Vec<_>>();
            Template::render("index", ImagesList { images: image_list })
        }
        Err(_) => Template::render("index", ImagesList { images: vec![]})
    }
}

async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build>  {
    let queries = [
        r#"
        CREATE TABLE IF NOT EXISTS images (
            id SERIAL UNIQUE, 
            path VARCHAR(150) NOT NULL, 
            width INTEGER NOT NULL, 
            height INTEGER NOT NULL, 
            title VARCHAR(200) NOT NULL, 
            private BOOLEAN DEFAULT FALSE, 
            content bytea NOT NULL )
            "#,
            r#"
        CREATE INDEX IF NOT EXISTS images_search_index 
            ON images 
            USING gin(to_tsvector('simple', title))
            "#,
            r#"
        CREATE TABLE IF NOT EXISTS comments (
            image_id INTEGER, 
            user_name VARCHAR(150), 
            comment VARCHAR(300), 
            CONSTRAINT fk_comment_image 
            FOREIGN KEY (image_id) 
            REFERENCES images(id))
            "#,
            r#"
        CREATE TABLE IF NOT EXISTS metadata (
            image_id INTEGER, 
            creationTime FLOAT, 
            camera_make VARCHAR(10000), 
            camera_model VARCHAR (10000), 
            orientation INTEGER, 
            horizontal_ppi INTEGER, 
            vertical_ppi INTEGER, 
            shutter_speed FLOAT, 
            color_space VARCHAR(20), 
            CONSTRAINT fk_metadata_images 
                FOREIGN KEY (image_id) 
                REFERENCES images(id))
            "#];
    let handle = MyPgDatabase::get_one(&rocket).await   
        .expect("Database mounted");
    for query in queries {
        handle.run(|conn| {
            conn.execute(query, &[])
        }).await
        .expect("Can't initialize the database");
    }
    rocket
}

fn get_database_url() -> String {
    if let Ok(url) = std::env::var("DATABASE_URL") {
        return url;
    } else {
        return String::from("postgres://postgres:rocket@127.0.0.1/postgres")
    }
}

#[launch]
fn rocket() -> _ {
    let db: Map<_, Value> = map! {
        "url" => get_database_url().into()
    };
    let figment = rocket::Config::figment()
        .merge(("databases", map!["postgres" => db]));
    
    rocket::custom(figment)
    .attach(MyPgDatabase::fairing())
    .attach(AdHoc::on_ignite("Postgres init", run_migrations))
    .attach(Template::fairing())
    .attach(CSP) // Attach CSP fairing here
    .mount("/static", FileServer::from("static"))
    .mount("/", routes![images, upload, upload_post, img, images_name, comment, search])
}

